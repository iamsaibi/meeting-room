import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MeetingDetails } from 'src/app/model/meeting.model';
import {
  CustomValidatorEndTime,
  CustomValidatorStartTime,
  CustomValidatorWeekendCheck,
} from 'src/app/model/vaidator';
import { DataProcessingService } from 'src/app/services/data-processing.service';

@Component({
  selector: 'app-book-meeting',
  templateUrl: './book-meeting.component.html',
  styleUrls: ['./book-meeting.component.css'],
})
export class BookMeetingComponent implements OnInit {
  rooms: any[];
  roomBookingForm: FormGroup;

  errorStatement: string;
  bookedMeetingsToDisplay: MeetingDetails[];

  constructor(private dataProcessingService: DataProcessingService) {
    this.rooms = new Array(10);
  }

  ngOnInit() {
    this.createBookingForm();
    this.viewMeetings();
    this.bookedMeetingsToDisplay = this.dataProcessingService.fetchDataForDateandRoom(
      this.date.value,
      this.meetingRoom.value
    );
  }

  /**
   * This function is invoked once user confirms the booking and clicks Booking button
   */
  bookNow() {
    if (!this.roomBookingForm.valid) {
      return;
    }

    this.dataProcessingService.addMeetingData({
      meetingId: this.dataProcessingService.randomIdGenerator(),
      date: this.date.value,
      fromTime: this.fromTime.value,
      toTime: this.toTime.value,
      username: this.username.value,
      meetingRoom: this.meetingRoom.value,
      meetingAgenda: this.meetingAgenda.value,
    });

    alert('Meeting Booked');
    this.date.setValue('');
    this.meetingRoom.setValue(0);
    this.bookedMeetingsToDisplay = this.dataProcessingService.fetchMeetingData();
  }

  /**
   * A callback method which will convert time (hr:mm) into minutes
   * @param value value in 'hr:mm' format to convert into minutes
   * @returns returns converted minutes
   */
  convertTimeIntoMinutes(value: string): number {
    let timeArr = value.split(':');
    return +timeArr[0] * 60 + +timeArr[1];
  }

  /**
   * A method to create room booking form
   */
  createBookingForm() {
    this.roomBookingForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      date: new FormControl(null, [CustomValidatorWeekendCheck, Validators.required]),
      fromTime: new FormControl('', [Validators.required]),
      toTime: new FormControl('', [Validators.required]),
      meetingRoom: new FormControl(0, [Validators.required]),
      meetingAgenda: new FormControl('', [Validators.required]),
    });

    this.toTime.setValidators(CustomValidatorEndTime(this.fromTime));

    this.fromTime.setValidators(CustomValidatorStartTime(this.toTime));

    this.fromTime.statusChanges.subscribe((status) => {
      if (status === 'INVALID') {
        if (
          this.fromTime.hasError('timeError') &&
          !this.fromTime.hasError('rangeError') &&
          !this.fromTime.hasError('pastTime')
        )
          this.errorStatement = 'Meeting duration min. 30mins';
      } else {
        this.errorStatement = '';
        this.toTime.setErrors(null, { emitEvent: false });
      }
    });
    this.toTime.statusChanges.subscribe((status) => {
      if (status === 'INVALID') {
        if (
          this.toTime.hasError('timeError') &&
          !this.toTime.hasError('rangeError') &&
          !this.toTime.hasError('pastTime')
        )
          this.errorStatement = 'Meeting duration min. 30mins';
      } else {
        this.errorStatement = '';
        this.fromTime.setErrors(null, { emitEvent: false });
      }
    });
  }

  /**
   * Updates table data on the basis of different filters
   */
  viewMeetings() {
    this.date.valueChanges.subscribe((changedValue) => {
      let room = this.meetingRoom.value;
      this.bookedMeetingsToDisplay = this.dataProcessingService.fetchDataForDateandRoom(
        changedValue,
        room
      );
    });
    this.meetingRoom.valueChanges.subscribe((changedValue) => {
      let date = this.date.value;
      this.bookedMeetingsToDisplay = this.dataProcessingService.fetchDataForDateandRoom(
        date,
        changedValue
      );
    });
  }

  get username() {
    return this.roomBookingForm.controls['username'];
  }

  get date() {
    return this.roomBookingForm.controls['date'];
  }

  get fromTime() {
    return this.roomBookingForm.controls['fromTime'];
  }

  get toTime() {
    return this.roomBookingForm.controls['toTime'];
  }

  get meetingRoom() {
    return this.roomBookingForm.controls['meetingRoom'];
  }

  get meetingAgenda() {
    return this.roomBookingForm.controls['meetingAgenda'];
  }
}
