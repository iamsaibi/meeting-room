import { Component, Input, OnChanges, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'protractor';
import { MeetingDetails } from 'src/app/model/meeting.model';
import { DataProcessingService } from 'src/app/services/data-processing.service';

@Component({
  selector: 'app-view-meetings',
  templateUrl: './view-meetings.component.html',
  styleUrls: ['./view-meetings.component.css'],
})
export class ViewMeetingsComponent implements OnInit, OnChanges {
  meetingsData: MeetingDetails[];

  @Input() bookedMeetings: MeetingDetails[];

  constructor(private dataProcessingService: DataProcessingService) {
    this.meetingsData = [];
  }

  ngOnInit() {
    this.meetingsData = this.dataProcessingService.fetchMeetingData();
  }

  ngOnChanges() {
    this.meetingsData = this.bookedMeetings;
  }

  /**
   * Deletes meeting with specific id
   * @param meetingId Booked meeting id
   */
  deleteMeeting(meetingId) {
    this.dataProcessingService.deleteMeetingData(meetingId);
    this.meetingsData = this.dataProcessingService.fetchMeetingData();
  }
}
