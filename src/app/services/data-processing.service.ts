import { Injectable } from '@angular/core';
import { MeetingDetails } from '../model/meeting.model';
import { getFromStorage, setStorage } from '../model/storage.utilities';

@Injectable({
  providedIn: 'root',
})
export class DataProcessingService {
  meetingsArray: MeetingDetails[];

  constructor() {
    this.meetingsArray = [];
  }

  /**
   * A method to add meeting's data on booking a meeting
   * @param data Meeting details
   */
  addMeetingData(data: MeetingDetails) {
    this.meetingsArray = getFromStorage('MeetingOccupancy');
    this.meetingsArray.push(data);
    setStorage('MeetingOccupancy', this.meetingsArray);
  }

  /**
   * A method which deletes object with specific id provided
   * @param meetingId Meeting ID
   */
  deleteMeetingData(meetingId: number) {
    let index = this.meetingsArray.findIndex((data) => data.meetingId == meetingId);
    if (index != -1) {
      this.meetingsArray.splice(index, 1);
    }
    setStorage('MeetingOccupancy', this.meetingsArray);
  }

  /**
   * A method which retrieves existing meeting data
   * @readonly an array of meeting data
   */
  fetchMeetingData() {
    this.meetingsArray = getFromStorage<MeetingDetails[]>('MeetingOccupancy');
    return this.meetingsArray;
  }

  /**
   * Generates 6 digit number
   * @returns 6 digit random number
   */
  randomIdGenerator(): number {
    return Math.floor(100000 + Math.random() * 900000);
  }

  /**
   * Gets meeting details for specific date and room
   * @param date Booked Date
   * @param room Room number
   */
  fetchDataForDateandRoom(date: string, room: string) {
    let data: MeetingDetails[] = getFromStorage('MeetingOccupancy');
    if (!date && !room) {
      return data;
    } else if (!date) {
      return data.filter((el) => el.meetingRoom == room);
    } else if (!room) {
      return data.filter((el) => el.date == date);
    }
    return data.filter((el) => el.date == date && el.meetingRoom == room);
  }
}
