/**
 * Model for Meeting Details
 */
export interface MeetingDetails {
  meetingId: number;
  username: string;
  date: string;
  fromTime: string;
  toTime: string;
  meetingRoom: string;
  meetingAgenda: string;
}
