/**
 * This function returns the parsed contacts data from localStorage
 */
export function getFromStorage<T>(key: string): T {
  return JSON.parse(localStorage.getItem(key));
}

/**
 * This function sets the localStorage for one or more contacts
 * @param contacts Contacts data or Contacts data array
 */
export function setStorage<T>(key: string, value: T) {
  localStorage.setItem(key, JSON.stringify(value));
}
