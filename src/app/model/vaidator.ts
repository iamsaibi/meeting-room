import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';

/**
 * Validates End time for meeting
 * @param abstractControl Abstract Control
 * @returns null | rangeError | pastTime |timeError
 */
export function CustomValidatorEndTime(abstractControl: AbstractControl) {
  return (control: AbstractControl) => {
    let endTime = dateConversion(control.value);
    if (endTime.getHours() < 9 || endTime.getHours() > 18) {
      return {
        rangeError: true,
      };
    }

    let currentTime = new Date().getTime();
    if (currentTime > endTime.getTime()) {
      return {
        pastTime: true,
      };
    }
    if (abstractControl.value) {
      let startTime = dateConversion(abstractControl.value);

      let startTotalMin = startTime.getHours() * 60 + startTime.getMinutes();
      let endTotalMin = endTime.getHours() * 60 + endTime.getMinutes();

      let diff = endTotalMin - startTotalMin;

      if (diff < 30)
        return {
          timeError: true,
        };
      return null;
    }
  };
}

/**
 * Validates Start time for meeting
 * @param abstractControl Abstract Control
 * @returns null | rangeError | pastTime |timeError
 */
export function CustomValidatorStartTime(abstractControl: AbstractControl) {
  return (control: AbstractControl) => {
    let startTime = dateConversion(control.value);
    if (startTime.getHours() < 9 || startTime.getHours() > 18) {
      return {
        rangeError: true,
      };
    }

    let currentTime = new Date().getTime();
    if (currentTime > startTime.getTime()) {
      return {
        pastTime: true,
      };
    }
    if (abstractControl.value) {
      let endTime = dateConversion(abstractControl.value);

      let startTotalMin = startTime.getHours() * 60 + startTime.getMinutes();
      let endTotalMin = endTime.getHours() * 60 + endTime.getMinutes();

      let diff = endTotalMin - startTotalMin;

      if (diff < 30)
        return {
          timeError: true,
        };
      return null;
    }
  };
}

/**
 * Validates date for weekend or previous dates
 * @param control abstract control
 * @returns null | dateError | weekendError
 */
export function CustomValidatorWeekendCheck(control: FormControl) {
  let today = new Date();
  let selectedDate = new Date(control.value);

  if (selectedDate < today) {
    return {
      dateError: true,
    };
  }

  let day = selectedDate.getDay();

  if (day === 0 || day === 6) {
    return {
      weekendError: true,
    };
  }
  return null;
}

/**
 * Converts time from 24hr format to minutes
 * @param time Time in 24 hr format as string
 * @returns number of minutes
 */
function dateConversion(time: string) {
  let t = time.split(':');
  let hr = t[0];
  let mm = t[1];
  let date = new Date();
  date.setHours(+hr);
  date.setMinutes(+mm);
  return date;
}
